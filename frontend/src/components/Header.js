import React from 'react'
import {Route} from 'react-router-dom'
import {useDispatch,useSelector} from 'react-redux'
import { LinkContainer } from 'react-router-bootstrap'
import { Navbar, Nav, Container, NavDropdown } from 'react-bootstrap'
import { logout } from '../actions/userActions'
import SearchBox from './SearchBox'

const Header = () => {

  const dispatch = useDispatch()
  
  
  
  const userLogin = useSelector(state => state.userLogin)
  const {userInfo} = userLogin
  
  
  const logoutHandler = () => {
    dispatch(logout())
  }
  return (
    <header>
      <Navbar bg='dark' variant='dark' expand='lg' collapseOnSelect  >
        <Container>
          <LinkContainer to='/'>
            <Navbar.Brand>Shopping king</Navbar.Brand>
          </LinkContainer>
          <Navbar.Toggle aria-controls='basic-navbar-nav' className="btn-outline-primary" />
          <Navbar.Collapse id='basic-navbar-nav' >
            <Route render={({history}) => <SearchBox history={history} />} />
            <Nav className='ml-auto'>
              <LinkContainer to='/cart'>
                <Nav.Link>
                  <i className='fas fa-shopping-cart'></i>Panier
                </Nav.Link>
              </LinkContainer>
              {userInfo ? (
                <NavDropdown title={userInfo.name} id="usernanme">
                  <LinkContainer to="/profile">
                    <NavDropdown.Item>
                      Profile
                    </NavDropdown.Item>
                  </LinkContainer>
                  <NavDropdown.Item onClick={logoutHandler}>
                    Deconnection
                  </NavDropdown.Item>
                </NavDropdown>
              ) : ( <LinkContainer to='/login'>
              <Nav.Link>
                <i className='fas fa-user'></i>Connexion
              </Nav.Link>
            </LinkContainer>
            )}
            {userInfo && userInfo.isAdmin && (
               <NavDropdown title="Admin" id="adminmenu">
               <LinkContainer to="/admin/userlist">
                 <NavDropdown.Item>
                   Profiles
                 </NavDropdown.Item>
               </LinkContainer>
               <LinkContainer to="/admin/productlist">
                 <NavDropdown.Item>
                   Produit
                 </NavDropdown.Item>
               </LinkContainer>
               <LinkContainer to="/admin/orderlist">
                 <NavDropdown.Item>
                   Commandes
                 </NavDropdown.Item>
               </LinkContainer>
             </NavDropdown>
            )}
             
            </Nav>
          </Navbar.Collapse>
        </Container>
      </Navbar>
    </header>
  )
}

export default Header
