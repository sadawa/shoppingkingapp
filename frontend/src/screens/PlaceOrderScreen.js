import React,{useEffect} from 'react'
import {Link} from "react-router-dom"
import {Button,Row,Col,ListGroup,Image,Card} from 'react-bootstrap'
import {useDispatch,useSelector} from "react-redux"
import Message from "../components/Message"
import CheckoutSteps from "../components/CheckoutSteps"
import {createOrder} from "../actions/orderActions"

const PlaceOrderScreen = ({history}) => {

    const dispatch = useDispatch()

    const cart = useSelector(state => state.cart)


    //Calculer le prix(taxe inventé)
    cart.itemsPrix = cart.cartItems.reduce((acc,item) => acc +item.prix * item.qty , 0 )

    const addDecimals = (num) => {
        return (Math.round(num*100) / 100).toFixed(2)
    }
    cart.shippingPrice = addDecimals(cart.itemsPrix > 60 ? 0 : 7)
    cart.taxPrice = addDecimals(Number((0.02 * cart.itemsPrix).toFixed(2)))
    cart.totalPrice = (Number(cart.itemsPrix) + Number(cart.shippingPrice) + Number(cart.taxPrice)).toFixed(2)

    const orderCreate = useSelector(state => state.orderCreate)
    const {order, success, error} = orderCreate

    useEffect(() => {
        if(success){
            history.push(`/order/${order._id}`)
        }
        // eslint-disable-next-line
    },[history,success])

    const placeOrderHandler = () => {
        dispatch(
            createOrder({
                orderItems: cart.cartItems,
                shippingAddress: cart.shippingAddress,
                paymentMethod: cart.paymentMethod,
                itemsPrix: cart.itemsPrix,
                shippingPrice: cart.shippingPrice,
                taxPrice: cart.taxPrice,
                totalPrice: cart.totalPrice,
        }))
    }

    return (
        <>
            <CheckoutSteps step1 step2 step3 step4/>
            <Row>
                <Col md={8}>
                    <ListGroup variant='flush'>
                        <ListGroup.Item>
                        <h2>Livraison</h2>
                        <p>
                            <strong>Adresse:</strong>
                            {cart.shippingAddress.address}, {cart.shippingAddress.city}{' '}
                        {cart.shippingAddress.postalCode},{' '}
                        {cart.shippingAddress.country}
                        </p>
                        </ListGroup.Item>
                        
                        <ListGroup.Item>
                            <h2>Type de Paiement</h2>
                            <strong>Méthodes:</strong>
                            {cart.paymentMethod}
                        </ListGroup.Item>
                   
                   <ListGroup.Item>
                    <h2>Panier </h2>
                    {cart.cartItems.length === 0 ? <Message>Panier vide</Message> : (
                        <ListGroup variant='flush'>
                            {cart.cartItems.map((item,index) => (
                                <ListGroup.Item key={index}>
                                    <Row>
                                        <Col md={1}>
                                            <Image src={item.image} alt={item.name} fluid rounded/>
                                        </Col>
                                        <Col>
                                        <Link to={`/product/${item.product}`}>
                                            {item.name}
                                        </Link>
                                        </Col>
                                        <Col md={4}>
                                            {item.qty} x {item.prix}€ = {item.qty * item.prix}€
                                        </Col>
                                    </Row>
                                </ListGroup.Item>
                            ))}
                        </ListGroup>
                    )}
                   </ListGroup.Item>
                   
                    </ListGroup>
                </Col>
                <Col md={4}>
                    <Card>
                    <ListGroup variant="flush">
                        <ListGroup.Item>
                            <h2>Resume</h2>
                        </ListGroup.Item>
                        <ListGroup.Item>
                            <Row>
                                <Col>Produit</Col>
                                <Col>{cart.itemsPrix}€</Col>
                            </Row>
                        </ListGroup.Item>
                        <ListGroup.Item>
                            <Row>
                                <Col>Livraison</Col>
                                <Col>{cart.shippingPrice}€</Col>
                            </Row>
                        </ListGroup.Item>
                        <ListGroup.Item>
                            <Row>
                                <Col>Taxe</Col>
                                <Col>{cart.taxPrice}€</Col>
                            </Row>
                        </ListGroup.Item>
                        <ListGroup.Item>
                            <Row>
                                <Col>Total</Col>
                                <Col>{cart.totalPrice}€</Col>
                            </Row>
                        </ListGroup.Item>
                                <ListGroup.Item>
                                   
                             {error && <Message variant="danger">{error}</Message>}
                                </ListGroup.Item>

                        <ListGroup.Item>
                            <Button type="button" className="btn-block" disabled={cart.cartItems === 0} onClick={placeOrderHandler}>Commander</Button>
                        </ListGroup.Item>
                    </ListGroup>
                    </Card>
                </Col>
            </Row>
        </>
    )
}

export default PlaceOrderScreen
