
import React,{useState,useEffect} from 'react'
import {Link} from "react-router-dom"
import {Form , Button} from 'react-bootstrap'
import {useDispatch,useSelector} from "react-redux"
import Message from "../components/Message"
import Loader from "../components/Loader"
import FromContainer from "../components/FromContainer"
import {listProductDetails,updateProduct} from "../actions/productActions"
import { PRODUCT_UPDATE_RESET } from '../constants/productContants'
import axios from 'axios'



const ProductEditScreen = ({match,history}) => {
    const productId = match.params.id

  const [name, setName] = useState('')
  const [prix, setPrix] = useState(0)
  const [image, setImage] = useState('')
  const [marque, setMarque] = useState('')
  const [category, setCategory] = useState('')
  const [stock, setStock] = useState(0)
  const [description, setDescription] = useState('')
  const [uploading, setUploading] = useState(false)
  

  const dispatch = useDispatch()

  const productDetails = useSelector((state) => state.productDetails)
  const { loading, error, product } = productDetails
 
  const productUpdate = useSelector((state) => state.productUpdate)
  const { loading: loadingUpdate, error:errorUpdate, success: successUpdate } = productUpdate
 
    
  useEffect(() => {
    if(successUpdate){
        dispatch({type: PRODUCT_UPDATE_RESET})
        history.push('/admin/productlist')
    }else {
        if (!product.name || product._id !== productId) {
            dispatch(listProductDetails(productId))
          } else {
            setName(product.name)
            setPrix(product.prix)
            setImage(product.image)
            setMarque(product.brand)
            setCategory(product.category)
            setStock(product.Stock)
            setDescription(product.description)
          }
    }
    
  }, [dispatch, history, productId, product,successUpdate])

  const uploadFileHandler = async (e) => {
    const file = e.target.files[0]
    const formData = new FormData()
    formData.append('image', file)
    setUploading(true)

    try {
      const config = {
        headers: {
          'Content-Type': 'multipart/form-data',
        },
      }

      const { data } = await axios.post('/api/upload', formData, config)

      setImage(data)
      setUploading(false)
    } catch (error) {
      console.error(error)
      setUploading(false)
    }
  }
  
  const submitHandler = (e) => {
    e.preventDefault()
    // mise a jour 
    dispatch(updateProduct({
        _id: productId,
        name,
        prix,
        image,
        marque,
        category,
        description,
        stock
    }))
  }

    return (
        <>
        <Link to="/admin/productlist" className="btn btn-light my-3">
            Retour
        </Link>
        <FromContainer>
            <h1>Modifie le produit</h1>
            {loadingUpdate && <Loader/>}
            {errorUpdate && <Message variant="danger">{errorUpdate}</Message>}
            {loading ? <Loader/> : error ? <Message variant="danger">{error}</Message> : (
                 <Form onSubmit={submitHandler}>
                <Form.Group controlId='name'>
              <Form.Label>Nom</Form.Label>
              <Form.Control
                type='name'
                placeholder='Entre le nom du produit'
                value={name}
                onChange={(e) => setName(e.target.value)}
              ></Form.Control>
            </Form.Group>

            <Form.Group controlId='prix'>
              <Form.Label>prix</Form.Label>
              <Form.Control
                type='number'
                placeholder='Entre le prix'
                value={prix}
                onChange={(e) => setPrix(e.target.value)}
              ></Form.Control>
            </Form.Group>

            <Form.Group controlId='image'>
              <Form.Label>Image</Form.Label>
              <Form.Control
                type='text'
                placeholder='Entre image du produit'
                value={image}
                onChange={(e) => setImage(e.target.value)}
              ></Form.Control>
                <Form.File
                id='image-file'
                label='Choose File'
                custom
                onChange={uploadFileHandler}
              ></Form.File>
              {uploading && <Loader />}
            </Form.Group>

            <Form.Group controlId='marque'>
              <Form.Label>Marque</Form.Label>
              <Form.Control
                type='text'
                placeholder='Entre la marque du produit'
                value={marque}
                onChange={(e) => setMarque(e.target.value)}
              ></Form.Control>
            </Form.Group>

            <Form.Group controlId='Stock'>
              <Form.Label> Stock</Form.Label>
              <Form.Control
                type='number'
                placeholder='Entre le nombre de Stock'
                value={stock}
                onChange={(e) => setStock(e.target.value)}
              ></Form.Control>
            </Form.Group>

            <Form.Group controlId='category'>
              <Form.Label>Categorie</Form.Label>
              <Form.Control
                type='text'
                placeholder='Entre le categorie du produit'
                value={category}
                onChange={(e) => setCategory(e.target.value)}
              ></Form.Control>
            </Form.Group>

            <Form.Group controlId='description'>
              <Form.Label>Description</Form.Label>
              <Form.Control
                type='text'
                placeholder='Entre la description du produit'
                value={description}
                onChange={(e) => setDescription(e.target.value)}
              ></Form.Control>
        
            </Form.Group>
     
                     <Button type="submit" variant="primary">
                        Modification
                     </Button>
                     </Form>
                 
            )}
           
        </FromContainer>
        </>
       
    )
}

export default ProductEditScreen