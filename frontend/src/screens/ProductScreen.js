import React, {useEffect,useState} from 'react'
import { Link } from 'react-router-dom'
import {useDispatch,useSelector} from "react-redux"
import { Row, Col, Image, ListGroup, Card, Button, Form } from 'react-bootstrap'
import Rating from '../components/Rating'
import {listProductDetails,createProductReview} from "../actions/productActions"
import Loader from '../components/Loader'
import Message from '../components/Message'
import Meta from '../components/Meta'
import {PRODUCT_CREATE_REVIEW_RESET} from "../constants/productContants"


const ProductScreen = ({ history,match }) => {
 const [qty , setQty] = useState(1)
 const [rating , setRating] = useState(0)
 const [comment , setComment] = useState("")
 
 
  const dispatch = useDispatch()

 const productDetails = useSelector(state => state.productDetails)
 const {loading,error,product} = productDetails
 
 
 const userLogin = useSelector(state => state.userLogin)
 const {userInfo} = userLogin

 
 const productReviewCreate = useSelector(state => state.productReviewCreate)
 const {error: errorProductReview,success:successProductReview} = productReviewCreate

  useEffect(() => {
    if(successProductReview){
      alert('Avis Envoye')
      setRating(0)
      setComment(" ")
      dispatch({type: PRODUCT_CREATE_REVIEW_RESET})
    }
      dispatch(listProductDetails(match.params.id))
   }, [dispatch,match,successProductReview])

  const addToCardHandler = () => {
  history.push(`/cart/${match.params.id}?qty=${qty}`)
  }
  const submitHandler = (e) => {
    e.preventDefault()
    dispatch(createProductReview(match.params.id, {
      rating,
      comment
    }))
  }
  return (
    <>
      <Link className='btn btn-primary my-3' to='/'>
        Retour
      </Link>
      {loading ? <Loader/> : error ? <Message variant="danger">{error}</Message> : (
             <>
             <Meta title={product.name}/>
             <Row>
             <Col md={6}>
               <Image src={product.image} alt={product.name} fluid />
             </Col>
             <Col md={3}>
               <ListGroup variant='flush'>
                 <ListGroup.Item>
                   <h3>{product.name}</h3>
                 </ListGroup.Item>
                 <ListGroup.Item>
                   <Rating
                    value={product.note}
                    text={`${product.avis} avis`}
                   />
                 </ListGroup.Item>
                 <ListGroup.Item>Prix: {product.prix}€</ListGroup.Item>
                 <ListGroup.Item>Description: {product.description}</ListGroup.Item>
               </ListGroup>
             </Col>
             <Col md={3}>
               <Card>
                 <ListGroup variant='flush'>
                   <ListGroup.Item>
                     <Row>
                       <Col>Prix:</Col>
                       <Col>
                         <strong>{product.prix}€</strong>
                       </Col>
                     </Row>
                   </ListGroup.Item>
                   <ListGroup.Item>
                     <Row>
                       <Col>Statut:</Col>
                       <Col>{product.stock > 0 ? 'Stock' : 'Epuisé'}</Col>
                     </Row>
                   </ListGroup.Item>

                  {product.stock > 0 && (
                    <ListGroup.Item>
                      <Row>
                        <Col>Qty</Col>
                        <Col>
                        <Form.Control as="select"  value={qty} onChange={(e) => setQty(e.target.value)}>
                          {[...Array(product.stock).keys()].map(x => (
                            <option key={x+1} value={x+1}>
                              {x+1} 
                            </option>
                          ))}
                        </Form.Control>
                        </Col>
                      </Row>
                    </ListGroup.Item>
                  )}

                   <ListGroup.Item>
                     <Button
                     onClick={addToCardHandler}
                       className='btn-block'
                       type='button'
                       disabled={product.stock === 0}>
                       Rajoute panier
                     </Button>
                   </ListGroup.Item>
                 </ListGroup>
               </Card>
             </Col>
           </Row>
           <Row>
             <Col md={3}>
                  <h2>Avis</h2>
                  {product.reviews.length === 0 && <Message>Pas Avis</Message>}
              <ListGroup variant='flush'>
                {product.reviews.map((review) => (
                  <ListGroup.Item key={review._id}>
                    <strong>{review.name}</strong>
                    <Rating value={review.rating} />
                    <p>{review.createdAt.substring(0, 10)}</p>
                    <p>{review.comment}</p>
                  </ListGroup.Item>
                ))}
                <ListGroup.Item>
                  <h2>Avis du client</h2>
                  {successProductReview && (
                    <Message variant='success'>
                      Avis envoyé
                    </Message>
                  )}
                  
                  {errorProductReview && (
                    <Message variant='danger'>{errorProductReview}</Message>
                  )}
                  {userInfo ? (
                    <Form onSubmit={submitHandler}>
                      <Form.Group controlId='rating'>
                        <Form.Label>Avis</Form.Label>
                        <Form.Control
                          as='select'
                          value={rating}
                          onChange={(e) => setRating(e.target.value)}
                        >
                          <option value=''>Selection...</option>
                          <option value='1'>1 - Nul</option>
                          <option value='2'>2 - Moyen</option>
                          <option value='3'>3 - Bien</option>
                          <option value='4'>4 - Trés Bien</option>
                          <option value='5'>5 - King</option>
                        </Form.Control>
                      </Form.Group>
                      <Form.Group controlId='comment'>
                        <Form.Label>Commentaire</Form.Label>
                        <Form.Control
                          as='textarea'
                          row='5'
                          value={comment}
                          onChange={(e) => setComment(e.target.value)}
                        ></Form.Control>
                      </Form.Group>
                      <Button
                        
                        type='submit'
                        variant='primary'
                      >
                        Envoyé
                      </Button>
                    </Form>
                  ) : (
                    <Message>
                      Veuillez <Link to='/login'> vous connectez</Link> pour ecrire un avis{' '}
                    </Message>
                  )}
                </ListGroup.Item>
              </ListGroup>
            </Col>
          </Row>
        </>
      )}
    </>
  )
}

export default ProductScreen
