import React,{useState,useEffect} from 'react'
import {Link} from "react-router-dom"
import {Form , Button, Row,Col} from 'react-bootstrap'
import {useDispatch,useSelector} from "react-redux"
import Message from "../components/Message"
import Loader from "../components/Loader"
import FromContainer from "../components/FromContainer"
import {register} from "../actions/userActions"

const RegisterScreen = ({location,history}) => {
    const [name, setName] = useState('')
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState("")
    const [confirmPassword, setconfirmPassword] = useState("")
    const [message, setMessage] = useState(null)
    
    const dispatch = useDispatch()
    
    const userRegister = useSelector(state => state.userRegister)
    const {loading,error,userInfo} = userRegister
    
    const redirect = location.search ? location.search.split("=")[1] : "/"
    
    useEffect(() => {
        if(userInfo){
            history.push(redirect)
        }
    }, [history,userInfo,redirect])
    
    const submitHandler = (e) => {
        e.preventDefault()
        if(password!== confirmPassword){
            setMessage("Mot de passe ne correspond pas")
        }else {

            dispatch(register(name,email,password))
        }
    }

    return (
        <FromContainer>
            <h1>Inscription</h1>
            {message && <Message variant="danger">{message}</Message>}
            {error && <Message variant="danger">{error}</Message>}
            {loading && <Loader/>}
            <Form onSubmit={submitHandler}>
            <Form.Group controlId="name">
                <Form.Label>Nom</Form.Label>
                <Form.Control type="name" placeholder="Entre ton nom" value={name}
                onChange={(e) => setName(e.target.value)}>

                </Form.Control>
                </Form.Group>
                
                
                
                <Form.Group controlId="email">
                <Form.Label>Email</Form.Label>
                <Form.Control type="email" placeholder="Entre ton address email" value={email}
                onChange={(e) => setEmail(e.target.value)}>

                </Form.Control>
                </Form.Group>

                 <Form.Group controlId="password">
                <Form.Label>Mot de passe</Form.Label>
                <Form.Control type="password" placeholder="Entre ton mot de passe" value={password}
                onChange={(e) => setPassword(e.target.value)}>

                </Form.Control>
                </Form.Group>

                <Form.Group controlId="confirmPassword">
                <Form.Label>Confirme mot de passe</Form.Label>
                <Form.Control type="password" placeholder="Confirme ton mot de passe" value={confirmPassword}
                onChange={(e) => setconfirmPassword(e.target.value)}>

                </Form.Control>
                </Form.Group>


                <Button type="submit" variant="primary">
                   Inscription
                </Button>
                
            </Form>
            <Row className="py-3">
                <Col>
                As tu un compte ? 
                 <Link to={redirect ?`/login?redirect=${redirect}` : "/login"}>Inscription</Link>
                Connection
                </Col>
            </Row>
        </FromContainer>
    )
}

export default RegisterScreen
