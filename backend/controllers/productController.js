import asyncHandler from 'express-async-handler'
import Product from "../models/productModel.js"

//@desc Prend tous les produit
//@route Get/api/products
//@access Public
const getProducts = asyncHandler(async(req,res) => {
    const pageSize = 10

   const page = Number(req.query.pageNumber) || 1

  const keyword = req.query.keyword
    ? {
        name: {
          $regex: req.query.keyword,
          $options: 'i',
        },
      }
    : {}

  const count = await Product.countDocuments({ ...keyword })
  const products = await Product.find({ ...keyword })
    .limit(pageSize)
    .skip(pageSize * (page - 1))

  res.json({ products, page, pages: Math.ceil(count / pageSize) })
})

//@desc Prend un produit
//@route Get/api/products/:id
//@access Public
const getProductById = asyncHandler(async(req,res) => {
    const product = await Product.findById(req.params.id)

    if (product) {
      res.json(product)
      }else{
          res.status(404)
          throw new Error("Product not found")
      }

    })

    
    //@desc Supprime un produit
    //@route DELETE/api/products/:id
    //@access Private/Admin
    const deleteProduct = asyncHandler(async(req,res) => {
        const product = await Product.findById(req.params.id)
        
        if (product) {
            await product.remove()
            res.json({message: 'Product removed'})
        }else{
            res.status(404)
            throw new Error("Product not found")
        }
        
    })
     //@desc Cree un produit
    //@route POST/api/products
    //@access Private/Admin
    const createProduct = asyncHandler(async(req,res) => {
        const product = new Product({
            name: "Sample name",
            prix: 0,
            user: req.user.id,
            image:"/images/sample.jpg",
            marque: 'Sample brand',
            category: "Sample category",
            stock: 0,
            note: 0, 
            description: "Sample description"
        })
        
        const createdProduct = await product.save()
        res.status(201).json(createdProduct)
        
   
    })
     //@desc Modifie un produit
    //@route PUT/api/products/:id
    //@access Private/Admin
    const updateProduct = asyncHandler(async(req,res) => {
        const {name,prix,description,image,marque,category,stock} = req.body
        
        const product = await Product.findById(req.params.id)
        
        
        if(product){
            product.name =name
            product.prix = prix
            product.description = description
            product.image = image
            product.marque = marque
            product.category = category
            product.stock = stock
            
            
            const updateProduct = await product.save()
            res.json(updateProduct)

        }else{
            res.status(404)
            throw new Error("Product not found")
        }
        
        
        
   
    })

    //@desc Cree un nouveau avis
    //@route POST/api/products/:id/review
    //@access Private
    const createProductReview = asyncHandler(async(req,res) => {
        const {rating,comment} = req.body
        
        const product = await Product.findById(req.params.id)
        
        
        if(product){
            const alreadyReviewed = product.reviews.find(r => r.user.toString() === req.user._id.toString())

            if(alreadyReviewed){
                res.status(400)
                throw new Error("Product alredy reviewed")
            }
            const review = {
                name: req.user.name,
                rating: Number(rating),
                comment,
                user: req.user._id
            }
            product.reviews.push(review)

            product.rating =
            product.reviews.reduce((acc, item) => item.rating + acc, 0) /
            product.reviews.length
           
             await product.save()

             res.status(201).json({message: "review added"})
        }else{
            res.status(404)
            throw new Error("Product not found")
        }
        
        
        
   
    })

    //@desc Prendre les tops produit
    //@route GET/api/products/top
    //@access Public
    const getTopProducts = asyncHandler(async(req,res) => {
        const products = await Product.find({}).sort({ rating: -1 }).limit(3)

  res.json(products)

   
    })
   
   
   
   
    export {
        getProducts,
        getProductById,
        deleteProduct,
        createProduct,
        updateProduct,
        createProductReview,
        getTopProducts
    }