import bcrypt from "bcryptjs"

const users = [
    {
        name: "Admin User",
        email: "admin@example.com",
        password: bcrypt.hashSync('123456',10),
        isAdmin: true
    },
    {
        name: "John doe",
        email: "john@example.com",
        password: bcrypt.hashSync('123456',10),
        
    },
    {
        name: "Julie Rox",
        email: "julie@example.com",
        password: bcrypt.hashSync('123456',10),
        
    },
]

export default users